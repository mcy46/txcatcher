module TxCatcher

  class Server < Goliath::API

    use Goliath::Rack::Params

    def response(env)
      uri = env["REQUEST_URI"]
      begin
        return route_for(uri)
      rescue BitcoinRPC::JSONRPCError => e
        LOGGER.report e.to_s + "\n" + e.backtrace.join("\n")
        return [400, {}, { error: e.data }.to_json]
      rescue Exception => e
        LOGGER.report e.to_s + "\n" + e.backtrace.join("\n")
        return [500, {}, { error: e.to_s }.to_json]
      end
    end

    def route_for(path)
      puts "[REQUEST: #{path}]"
      if path =~ /\A\/addr\/[0-9a-zA-Z]+\/utxo/
        utxo(path)
      elsif path.start_with? "/addr/"
        address(path)
      elsif path.start_with? "/tx/send"
        broadcast_tx(params["rawtx"])
      elsif path.start_with? "/tx/"
        tx(path)
      elsif path.start_with? "/feerate"
        feerate(params["blocks_target"] || 2)
      elsif path == "/" || path.empty?
        version = File.read(File.expand_path(File.dirname(__FILE__) + "../../../VERSION"))
        [200, {}, "TxCatcher server v#{version.strip}, #{TxCatcher::Config.rpcnode["name"]}, current_block_height: #{TxCatcher.current_block_height}"]
      else
        [404, {}, { error: "404, not found" }.to_json]
      end
    end

    def address(path)
      path = path.sub(/\?.*/, '').split("/").delete_if { |i| i.empty? }
      addr = path.last

      address  = Address.find_or_create(address: addr)
      unless address.deposits.empty?
        deposits       = Deposit.where(address_id: address.id)
        deposits_count = deposits.count
        deposits       = deposits.eager(:transaction).limit(params["limit"] || 100)
        transactions   = deposits.map { |d| d.transaction }
        deposits = deposits.map do |d|
          t = d.transaction
          t.update(protected: true) unless t.protected
          t.update_block_height
          result = {
            txid:          t.txid,
            amount:        d.amount_in_btc,
            satoshis:      d.amount,
            confirmations: t.confirmations,
            block_height:  t.block_height,
          }
          result.merge!({ rbf: "yes", rbf_previous_txid: t.rbf_previous_transaction.txid }) if t.rbf?
          result
        end
        return [200, {}, { address: address.address, received: address.received, deposits_count: deposits_count, deposits_shown: deposits.size, deposits: deposits }.to_json]
      else
        return [200, {}, { address: addr, received: 0, deposits: [] }.to_json]
      end
    end

    def utxo(path)
      path = path.sub(/\?.*/, '').split("/").delete_if { |i| i.empty? }
      path.pop
      addr = path.last

      address  = Address.find_or_create(address: addr)
      deposits = Deposit.where(address_id: address.id).limit(params["limit"] || 100).eager(:transaction)
      return [200, {}, "{}"] if address.deposits.empty?

      transactions = deposits.map { |d| d.transaction }
      transactions.sort!          { |t1,t2| t2.created_at <=> t1.created_at }

      transactions.each do |t|
        # If we see a transaction with 0 confirmations, let's check if it got any news confirmations
        # by querying Bitcoind RPC.
        t.update_block_height_from_rpc if t.confirmations == 0
        # If still not confirmed, let's make it protected so it's not deleted during cleanup.
        t.protected = true if t.confirmations == 0
      end

      Transaction.update_all(transactions) # will only update the ones that changed!

      utxos = transactions.map do |t|
        outs = t.tx_hash["vout"].select { |out| out["scriptPubKey"]["addresses"] == [addr] }
        outs.map! do |out|
          out["confirmations"]   = t.confirmations || 0
          out["txid"]            = t.txid
          if t.rbf?
            out["rbf"]               = "yes"
            out["rbf_previous_txid"] = t.rbf_previous_transaction.txid
          end
          out
        end
        outs
      end.flatten

      return [200, {}, utxos.to_json]
    end

    def tx(path)
      path = path.sub(/\?.*/, '').split("/").delete_if { |i| i.empty? }
      txid = path.last
      tx = Transaction.find_or_create_from_rpc(txid)

      if tx && (!tx.deposits.empty? || tx.rbf?)
        tx.update_block_height! if tx.block_height.nil?
        return [200, {}, tx.to_json]
      else
        return [404, {}, "Transaction not found"]
      end
    end

    def broadcast_tx(txhex)
      TxCatcher.rpc_node.sendrawtransaction(txhex)
      tx = TxCatcher.rpc_node.decoderawtransaction(txhex)
      return [200, {}, tx.to_json]
    end

    def feerate(blocks_target)
      result = TxCatcher.rpc_node.estimatesmartfee(blocks_target.to_i)

      # This is for older versions of bitcoind/litecoind clients
      if result.to_s.include?("error")
        result = TxCatcher.rpc_node.estimatefee(blocks_target.to_i)
        result = { "feerate" => result, "blocks" => blocks_target}
      end

      return [200, {}, result["feerate"].to_s]
    end

  end

end
