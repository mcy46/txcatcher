Sequel.migration do
  change do

    alter_table :transactions do
      # Refers to the Transaction model primarykey
      add_column :rbf_previous_transaction_id, Bignum, index: true
      add_column :rbf_next_transaction_id, Bignum, index: true
      add_column :inputs_outputs_hash, String, index: true
    end

    alter_table :deposits do
      add_column :rbf_transaction_ids, String, text: true
    end

  end
end
